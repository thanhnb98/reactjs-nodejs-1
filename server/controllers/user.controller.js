const dbPool = require('../configs/db.config');

//[{"id":1,"code":"thanhnb","fullname":"nguyen ba thanh","email":"thanhnb.iist@gmail.com","password":"thanhnb","is_active":1}]
module.exports.getAll = async function (request, response) {
    let dbConnection = null;
    try {
        dbConnection = await dbPool.getConnection();
        const query = "SELECT * FROM sys_user su WHERE su.is_active = 1";
        const result = await dbConnection.query(query);
        response.send(result);
    } catch (error) {
        throw error;
    } finally {
        if (dbConnection != null) return dbConnection.release();
    }
}