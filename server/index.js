const express = require('express')
const app = express();
const port = 8080
const userRouter = require('./routers/user.route');
const cors = require('cors');

app.use(cors());
app.use('/users', userRouter);


app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})