// import mariadb
const mariadb = require('mariadb');

// create connection pool
const pool = mariadb.createPool({
    host: "localhost", //192.168.100.103
    user: "root", //sypt
    password: "password",  //123456a@
    database: "thanhnb" //thanhnb
});

// expose the ability to create new connections
module.exports = {
    getConnection: function () {
        return new Promise(function (resolve, reject) {
            pool.getConnection().then(function (connection) {
                resolve(connection);
            }).catch(function (error) {
                reject(error);
            });
        });
    }
}