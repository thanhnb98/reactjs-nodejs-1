const express = require('express');
const router = express.Router();
const userController = require('../controllers/user.controller');
// get all user
router.get('/get-all', userController.getAll);

module.exports = router;