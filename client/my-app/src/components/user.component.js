import React, { useState, useEffect } from "react";

function UserComponent() {
  const [userList, setUserList] = useState([]);

  useEffect(async () => {
    const getUsers = async () => {
      const userFromServer = await fetchUsers();
      setUserList(userFromServer);
    };
    getUsers();
  }, []);

  // fetch users
  const fetchUsers = async () => {
    const response = await fetch("http://localhost:8080/users/get-all");
    if (response.ok) {
      const users = await response.json();
      return users;
    } else {
      return [];
    }
  };

  return (
    <div>
      <h1>Hello</h1>
      {userList.map((user) => {
        return (
          <div>
            id: {user.id} | code: {user.code} | fullName: {user.fullname} |
            email: {user.email}
          </div>
        );
      })}
    </div>
  );
}

export default UserComponent;
