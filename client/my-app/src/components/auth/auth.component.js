import React, { useState, useEffect } from "react";

const initState = { email: "", password: "" };

function AuthComponent() {
  /**
   * ####################### STATE #############################
   * ###########################################################
   */
  const [formData, setFromData] = useState(initState);
  /**
   * ####################### Handle Method ####################
   * ##########################################################
   */
  const handleSubmit = (event) => {
    event.preventDefault();
    console.log("handleSubmit");
    console.log(formData);
  };
  const handleChange = () => {
    console.log("handleChange");
  };

  return (
    <div>
      <h1>Login form</h1>
      <form onSubmit={handleSubmit}>
        <div>
          <label>Email:</label>
          <input name="email" type="text" onChange={handleChange}></input>
        </div>
        <div>
          <label>Password:</label>
          <input
            name="password"
            type="password"
            onChange={handleChange}
          ></input>
        </div>
        <div>
          <button type="submit">Login</button>
        </div>
      </form>
    </div>
  );
}

export default AuthComponent;
