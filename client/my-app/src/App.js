import "./App.css";
import UserComponent from "../src/components/user.component";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import AuthComponent from "../src/components/auth/auth.component";

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={AuthComponent} />
        <Route path="/users" exact component={UserComponent} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
